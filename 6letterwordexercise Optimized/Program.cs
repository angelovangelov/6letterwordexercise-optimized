﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace _6letterwordexercise_Optimized {
  class Program {
    private static readonly string PathToFile = Directory.GetCurrentDirectory() + @"\input.txt";

    static void Main(string[] args) {
      var lines = GetTextLines();
      FindAllWordsCombinations(lines);
    }

    /// <summary>
    /// Facade method, which finds all words combination by given lines
    /// </summary>
    /// <param name="lines">input.txt file lines</param>
    private static void FindAllWordsCombinations(IEnumerable<string> lines) {

      //Hashset, because we don't care if word is duplicated --> we need to show single match anyway
      var words = new HashSet<string>();
      var syllables = new List<string>();

      foreach (var line in lines) {
        DetermineLineType(words, syllables, line);
      }

      //key is word, value is sillable and thus {key - value = other syllable}
      //which gives me {value + (key - value) = value}
      var missingSyllables = new Dictionary<string, string>();
      var syllableCount = syllables.Count;

      foreach (var word in words) {
        var matchIndex = CheckForSyllableMatch(word, syllables, missingSyllables, syllableCount);
        var matchExists = matchIndex > -1;
        if (matchExists) {
          CheckForCombination(word, syllables, missingSyllables, syllableCount, matchIndex);
        }
      }
    }

    /// <summary>
    /// Checks for combination by assuming we have single syllable match.
    /// </summary>
    /// <param name="word"></param>
    /// <param name="syllables"></param>
    /// <param name="missingSyllables"></param>
    /// <param name="syllableCount"></param>
    /// <param name="matchIndex"></param>
    private static void CheckForCombination(string word, List<string> syllables, Dictionary<string, string> missingSyllables, int syllableCount, int matchIndex) {
      if (syllableCount - 1 == matchIndex) {
        return;
      }

      //This is here in order to execute this check only one time.
      var isStartOfWordMissing = word.EndsWith(missingSyllables[word]);

      if (isStartOfWordMissing) {
        FindStartCombination(word, syllables, syllableCount, matchIndex, missingSyllables);
      }
      else {
        FindEndCombination(word, syllables, syllableCount, matchIndex, missingSyllables);
      }
    }

    /// <summary>
    /// Looking for missing syllable in the end of the word by assuming the start syllable exists.
    /// </summary>
    /// <param name="word"></param>
    /// <param name="syllables"></param>
    /// <param name="syllableCount"></param>
    /// <param name="matchIndex"></param>
    /// <param name="missingSyllables"></param>
    private static void FindEndCombination(string word, List<string> syllables, int syllableCount, int matchIndex, Dictionary<string, string> missingSyllables) {
      var missingSyllable = word.Remove(0, missingSyllables[word].Length);
      
      for (int i = matchIndex + 1; i < syllableCount; i++) {
        if (syllables[i] == missingSyllable) {
          Console.WriteLine($"{missingSyllables[word]} + {missingSyllable} = {word}");
          return;
        }
      }
    }

    /// <summary>
    /// Looking for missing syllable in the start of the word by assuming the end syllable exists.
    /// </summary>
    /// <param name="word"></param>
    /// <param name="syllables"></param>
    /// <param name="syllableCount"></param>
    /// <param name="matchIndex"></param>
    /// <param name="missingSyllables"></param>
    private static void FindStartCombination(string word, List<string> syllables, int syllableCount, int matchIndex, Dictionary<string, string> missingSyllables) {
      var missingSyllableLength = word.Length - missingSyllables[word].Length;
      var missingSyllable = word.Substring(0, missingSyllableLength);
      for (int i = matchIndex + 1; i < syllableCount; i++) {
        if (syllables[i] == missingSyllable) {
          Console.WriteLine($"{missingSyllable} + {missingSyllables[word]} = {word}");
          return;
        }
      }
    }

    /// <summary>
    /// Looking for syllable match and returning the index of the syllable in order not to iterate from the beginning 
    /// when we start to look for the 2nd syllable.
    /// </summary>
    /// <param name="word"></param>
    /// <param name="syllables"></param>
    /// <param name="missingSyllables"></param>
    /// <param name="syllableCount"></param>
    /// <returns></returns>
    private static int CheckForSyllableMatch(string word, List<string> syllables, Dictionary<string, string> missingSyllables, int syllableCount) {
      for (int i = 0; i < syllableCount; i++) {
        var syllable = syllables.ElementAt(i);
        if (word.StartsWith(syllable) || word.EndsWith(syllable)) {
          missingSyllables.Add(word, syllable);
          return i;
        }
      }
      return -1;
    }

    /// <summary>
    /// If the line is 6 characters long it's a word, otherwise it's a syllable.
    /// </summary>
    /// <param name="words"></param>
    /// <param name="syllables"></param>
    /// <param name="line"></param>
    private static void DetermineLineType(HashSet<string> words, List<string> syllables, string line) {
      if (line.Length == 6) { 
        words.Add(line);
      }
      else {
        syllables.Add(line);
      }
    }

    /// <summary>
    /// Reading the text file to the end.
    /// </summary>
    /// <returns></returns>
    private static IEnumerable<string> GetTextLines() {
      using (var file = new StreamReader(PathToFile)) {
        string line;

        while ((line = file.ReadLine()) != null) {
          yield return line;
        }
        file.Close();
      }
    }
  }
}